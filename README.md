# Deezgo

Deezgo is a music downloader that uses the deezer API to get the songs and
metadata.

## Installation

Run:
```
~ $ go get gitlab.com/heyitscassio/deezgo
```

And it will download and compile the program to `$GOPATH/bin`. Make sure to add
this directory to your PATH or copy the binary to your PATH.

## Configuration

The in first time running the program, it will ask for your arl 
([how to get it](https://deezloader.net/how-to-use-deezloader/) and it will put
 it in a json, at "$XDG_CONFIG_HOME"/deezgo/config.json, with the default
 configuration, like this:

```json
{
	"arl": "your arl here",
	"defaultpath": "."
}
```

### Description of the configuration options

- arl: your arl. (see above for an explanation)
- defaultpath: the default dir for downloading albuns, tracks or artists.
	- another value could be "/home/user/Music", for example.

## Commands

To download a track in the PWD, in the mp3 format use 

```sh
~ $ deezgo get -f mp3 https://www.deezer.com/us/track/97206056
```

Or in flac

```sh
~ $ deezgo get -f flac https://www.deezer.com/us/track/97206056
```

To use another directory

```sh
~ $ deezgo get -f mp3 -p ~/Music https://www.deezer.com/us/track/97206056
```

The same idea can be used to download an entire album, or entire discography of
an artist. But deezgo will make an directory when downloading an album, like
this:

```sh
~ $ deezgo get -f mp3 -p /tmp/Music https://www.deezer.com/us/album/13579923
~ $ ls /tmp/Music
'Agalloch - Ashes Against The Grain (Remastered)'
```

To search for an album

```sh
~ $ deezgo search yellow submarine
```

Or 

```sh
~ $ deezgo search -a yellow submarine
```

To search for a track

```sh
~ $ deezgo search -t yellow
```

Or artist

```sh
~ $ deezgo search -A slowdive
```

The output will be a table separated list, made to look decent and easily
parsable with awk(1). This one liner illustrates how it can be used in a script.

```sh
~ $ deezgo search -t yellow | fzf | awk '{print $NF}' | xargs -r deezgo get -p ~/Music
```
